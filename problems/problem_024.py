# Complete the calculate_average function which accepts
# a list of numerical values and returns the average of
# the numbers.
#
# If the list of values is empty, the function should
# return None
#
# Pseudocode is available for you

#In java there has no None,but has null.Example text=[]
#In python there has no NUll, None is a class in python

def calculate_average(values):
    count=0
    if len(values) == 0:
        return None
    else:
        for i in values:
            count+=i
        average = count/len(values)
        return average

test =[1,2,3]
print(calculate_average(test))
