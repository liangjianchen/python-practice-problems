# Complete the find_second_largest function which accepts
# a list of numerical values and returns the second largest
# in the list
#
# If the list of values is empty, the function should
# return None
#
# If the list of values has only one value, the function
# should return None
#
# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

def find_second_largest(values):
    result = []
    if len(values)<= 2:
        return None
    else:
        for i in values:
            if i not in result:
                result.append(i)
        first_largest = max(result)
        result.remove(first_largest)
        return max(result)

test=[10,30,30,30,20]
print(find_second_largest(test))
