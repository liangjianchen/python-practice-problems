# Complete the is_palindrome function to check if the value in
# the word parameter is the same backward and forward.
#
# For example, the word "racecar" is a palindrome because, if
# you write it backwards, it's the same word.

# It uses the built-in function reversed and the join method
# for string objects.

# Do some planning in ./planning.md

# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

# reversed is slow, and need to use joint to connecet each character after reversed
def is_palindrome2(word2):
    back_word = "".join(reversed(word2))
    if word2 == back_word:
        return "palindrome"
    else:
        return "No a palindrome"



#best solution
def is_palindrome(word):
    backword = word[::-1]
    if word == backword:
        return "palindrome"
    else:
        return "No a palindrome"



print(is_palindrome2('abbakh'))
