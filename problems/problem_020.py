# Complete the has_quorum function to test to see if the number
# of the people in the attendees list is greater than or equal
# to 50% of the number of people in the members list.

#attendees is a list, so need to calculate the len
def has_quorum(attendees_list, members_list):
    if len(attendees_list) >= 0.5 * len(members_list):
        return True
    else:
        return False
