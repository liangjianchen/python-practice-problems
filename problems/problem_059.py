# Write a function that meets these requirements.
#
# Name:       specific_random
# Parameters: none
# Returns:    a random number between 10 and 500, inclusive,
#             that is divisible by 5 and 7
#
# Examples:
#     * returns: 35
#     * returns: 105
#     * returns: 70
#
# Guidance:
#   * Generate all the numbers that are divisible by 5
#     and 7 into a list
#   * Use random.choice to select one

import random
def specific_random():
    nums = range(10,501)
    good_num = []
    for num in nums:
        if num % 5 == 0 or num % 7 == 0:
            good_num.append(num)
    return random.choice(good_num)



print(specific_random())
